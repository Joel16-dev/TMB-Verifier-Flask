import os
from selenium import webdriver
from sys import platform


def tmb_verifier_init():
    if platform == 'linux':
        return 'linux/chromedriver'
    elif platform == 'win32' or platform == 'cygwin':
        return 'windows/chromedriver.exe'
    elif platform == 'darwin':
        return 'mac/chromedriver'


chrome_driver = 'drivers/' + tmb_verifier_init()
os.environ["webdriver.chrome.driver"] = chrome_driver
browser = None


def tmb_verifier_get_driver():
    global browser
    browser = browser or webdriver.Chrome(chrome_driver)
    return browser
