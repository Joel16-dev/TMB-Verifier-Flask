from flask import Flask, request
from init import tmb_verifier_get_driver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from util import utils_capture_screenshot
import re
import time

app = Flask(__name__)


def find_between(s, first, last):
    try:
        start = s.rindex(first) + len(first)
        end = s.rindex(last, start)
        return s[start:end]
    except ValueError:
        return ""


def tmb_verifier_get_updated_html(browser, xpath):
    # HTML from `<html>`
    browser.execute_script("return document.documentElement.innerHTML;")

    # HTML from `<body>`
    browser.execute_script("return document.body.innerHTML;")

    # HTML from element with some JavaScript
    # element = browser.find_element_by_xpath('//td[@valign="' + class_name + '"]')
    element = browser.find_element_by_xpath(xpath)
    browser.execute_script("return arguments[0].innerHTML;", element)

    # HTML from element with `get_attribute`
    # element = browser.find_element_by_xpath('//td[@valign="' + class_name + '"]')
    element = browser.find_element_by_xpath(xpath)
    return element.get_attribute('innerHTML')


jquery_header = "\n\n<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>\n\n"
js_func = jquery_header + "<script>var row_index; $('#table tr').click(function(){ $(this).addClass('selected')." \
                          "siblings().removeClass('selected'); row_index = $(this).closest('tr').index(); " \
                          "var val=($(this).find('td:first').html()) + ($(this).find('td:nth-child(2)').html()); " \
                          "$.ajax({type: 'POST', url: '/getData', data: {entry_id: row_index, entry_name: val}, " \
                          "success: function(response) {$('#table').toggle(); $('#response').html(response);}}); });" \
                          "</script>"


def tmb_verifier_fix_table(data):
    fixed_table = find_between(data, 'style="color:Black;height:73px;border-collapse:collapse;">', '</table>')
    fixed_table = fixed_table[:-121]  # Get the data between the tags <table></table>
    fixed_table = re.sub('<td>\n', '', fixed_table)
    fixed_table = re.sub('</a>', '', fixed_table)
    fixed_table = re.sub('style="color:Blue;">', 'style="color:Blue;"></a><td>', fixed_table)
    fixed_table = re.sub('<a.*?</a>', '', fixed_table)
    fixed_table = re.sub('<tbody>', '', fixed_table)
    fixed_table = re.sub('&nbsp;', 'N/A', fixed_table)  # replace "&nbsp;" to N/A
    fixed_table = '<table>' + fixed_table + '</table>'
    fixed_table = re.sub('</tr>', '<td><button class="select_btn">Select</button></td></tr>', fixed_table)
    fixed_table = fixed_table[339:]
    fixed_table = '<div id="response"></div><table id="table"><tr style="color:White;background-color:#507CD1;' \
                  'font-weight:bold;"><th align="left" scope="col">Name</th><th align="left" scope="col">License</th>' \
                  '<th align="left" scope="col">Type</th><th align="left" scope="col">Address</th><th align="left" ' \
                  'scope="col">City</th><th align="left" scope="col">Selection</th></tr>' + fixed_table

    return fixed_table


@app.route("/getData", methods=['POST'])
def get_data():
    if request.method == 'POST':
        row_index = request.form.get('entry_id')
        file_name = request.form.get('entry_name')
        file_name = re.sub(', ', ' ', file_name)
        file_name = re.sub('\n', '', file_name)
        file_name = file_name.replace(' ', '')
        # print(file_name + row_index)

        selection = int(row_index) + 1

        if selection < 10:
            selected_id: str = "//a[contains(@href, 'ctl00$BodyContent$gvSearchResults$ctl0" + str(selection) + \
                               "$ctl00')]"
        else:
            selected_id: str = "//a[contains(@href, 'ctl00$BodyContent$gvSearchResults$ctl" + str(selection) + \
                               "$ctl00')]"

        select_item = tmb_verifier_get_driver().find_element_by_xpath(selected_id)
        select_item.click()

        # print(selected_id)
        verification_page = tmb_verifier_get_updated_html(tmb_verifier_get_driver(), '//td[@valign="top"]')
        utils_capture_screenshot(tmb_verifier_get_driver(), file_name + '.png')
        tmb_verifier_get_driver().close()
        return verification_page


@app.route("/", methods=['GET', 'POST'])
def index():
    global result_table
    fname = request.args.get('fname')
    lname = request.args.get('lname')
    # print(fname)
    # print(lname)

    tmb_verifier_get_driver().get('https://public.tmb.state.tx.us/HCP_Search/searchinput.aspx')

    WebDriverWait(tmb_verifier_get_driver(), 10).until(ec.element_to_be_clickable((By.ID, "BodyContent_btnAccept")))
    time.sleep(1)

    btn_accept = tmb_verifier_get_driver().find_element_by_id('BodyContent_btnAccept')
    btn_accept.click()

    if fname is not None and lname is not None:
        first_name = tmb_verifier_get_driver().find_element_by_id('BodyContent_tbFirstName')
        first_name.send_keys(fname)

        last_name = tmb_verifier_get_driver().find_element_by_id('BodyContent_tbLastName')
        last_name.send_keys(lname)

        btn_search = tmb_verifier_get_driver().find_element_by_id('BodyContent_btnSearch')
        btn_search.click()

        html = tmb_verifier_get_updated_html(tmb_verifier_get_driver(), '//div[@class="BaseForm"]')

        table_exists = tmb_verifier_get_driver().find_elements(By.XPATH, "//a[contains(@href, 'ctl00$BodyContent$gv"
                                                                         "SearchResults$ctl02$ctl00')]")

        if not table_exists:
            return jquery_header + '<script>alert("No records exist for this person");</script>'
        else:
            result_table = tmb_verifier_fix_table(html)
            # print(result_table)
            return result_table + js_func


if __name__ == '__main__':
    app.run(debug=True)
